package Two;

import java.util.Date;

public class Consumer implements Runnable {
	
	private static final int DELAY = 1;
	private Queue q;
	private String item;
	private int count;
	
	public Consumer(Queue aqueue, String aitem, int acount){
		q = aqueue;
		item = aitem;
		count = acount;
	}
	
	public void run() {
		
		try {
			for (int i = 1; i <= count; i++){
				Date now = new Date();
				String item = q.removeQueue();
				System.out.println(now+ " Remove item: " +item);
				Thread.sleep(DELAY);
				
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
}


}
