package Two;

import java.util.ArrayList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Queue {
	private ArrayList<String> qlist;
	private Lock lockQueue;
	private Condition lockCondition;
	
	public Queue(){
		qlist = new ArrayList<String>();
		lockQueue = new ReentrantLock();
		lockCondition = lockQueue.newCondition();
	}
	
	public void addQueue(String str) throws InterruptedException{
		lockQueue.lock();
		try{

			qlist.add(str);
			lockCondition.signalAll();
		}
		finally{
			lockQueue.unlock();
		}
	}
	
	public String removeQueue() throws InterruptedException{
		lockQueue.lock();
		String item;
		try{
			while (qlist.size() < 10){
				lockCondition.await();				
			}
			item = qlist.remove(0);
			lockCondition.signalAll();
			
		}
		finally{
			lockQueue.unlock();
		}		
		return item;	

	}
	
	public String getQueue(){
		String ans = "";
		for(String s: qlist){
			ans = ans+s+" ";
		}	
		return ans;
	}
	
	

}
