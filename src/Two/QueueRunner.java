package Two;

import java.util.ArrayList;

public class QueueRunner {

	public static void main(String[] args) {
		Queue q = new Queue();
		final String ITEM = "Box";
		final int THREADS = 100;
		
		for (int i = 1; i <= THREADS; i++){
			Producer p = new Producer(q, ITEM, THREADS);
			Consumer c = new Consumer(q, ITEM, THREADS);
			
			Thread pt = new Thread(p);
			Thread ct = new Thread(c);
			
			pt.start();
			ct.start();
		}	

	}

}
