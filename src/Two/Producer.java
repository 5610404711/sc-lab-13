package Two;

import java.util.Date;
import java.util.LinkedList;

public class Producer implements Runnable{
	private static final int DELAY = 1;
	private Queue q;
	private String item;
	private int count;
	
	public Producer(Queue aqueue, String aitem, int acount){
		q = aqueue;
		item = aitem;
		count = acount;
	}

	@Override
	public void run() {
			try {
				for (int i = 1; i <= count; i++){
					Date now = new Date();
					q.addQueue(item);
					System.out.println(now + " Add item: "+item);
					Thread.sleep(DELAY);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	}

}
