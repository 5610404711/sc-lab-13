package One;

import java.util.LinkedList;

public class MyRunnable {
	

	public static void main(String[] args) {
		LinkedList<Integer> linklist = new LinkedList<Integer>();
		final int ITEM = 10;
		final int THREADS = 100;
		
		System.out.print("Before: ");
		
		for(int l: linklist){
			System.out.print(l+" ");
		}
		
		System.out.println("");
		
		for (int i = 1; i <= THREADS; i++){
			AddRunnable add = new AddRunnable(linklist, ITEM, THREADS);
			RemoveRunnable remove = new RemoveRunnable(linklist, ITEM, THREADS);
			
			Thread at = new Thread(add);
			Thread rt = new Thread(remove);
			
			at.start();
			rt.start();
		}
		System.out.print("After: ");
		
		for(int l: linklist){
			System.out.print(l+" ");
		}
		

	}

}
